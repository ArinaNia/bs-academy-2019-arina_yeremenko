class NewPlace {

    get namePlaceInput () {return $('input.input.is-medium')};
    get zipInput () {return $('input[placeholder="09678"]')};
    get addressInput () {return $('input[placeholder="Khreschatyk St., 14"]')};
    get phoneInput () {return $('input[placeholder="+380961112233"]')};
    get siteURLInput () {return $('input[placeholder="http://the-best-place.com/"]')};
    get descriptionInput () {return $('textarea.textarea')};
    get buttonNext () {return $('div.tab-item:not([style="display: none;"]) span.is-success')};
    get uploadImagePlaceInput () {return $('input[type=file]')};
    get openCategory () {return  $('.select')};
    get selectCategory () {return $('select > option:nth-child(4)')};
    get openTag () {return $('div:nth-child(2) > div > div > div > span')};
    get selectTag () {return $('select > option:nth-child(2)')};
    get checkFeature () {return $('label > span.check.is-success')};
    get getNamePlace () {return $('div.place-venue__place-name')}
};

module.exports = NewPlace;