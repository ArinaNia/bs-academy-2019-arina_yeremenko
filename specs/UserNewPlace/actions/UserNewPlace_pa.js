const NewPlacePage = require('../page/UserNewPlace_po');
const page = new NewPlacePage();

class NewPlaceActions {

    enterPlaceName(value) {
        page.namePlaceInput.waitForExist(2000);
        page.namePlaceInput.setValue(value);
    }

    enterZip(value) {
        page.zipInput.waitForDisplayed(2000);
        page.zipInput.setValue(value);
    }

    enterAddress(value) {
        page.addressInput.waitForDisplayed(2000);
        page.addressInput.setValue(value);
    }

    enterPhone(value) {
        page.phoneInput.waitForDisplayed(2000);
        page.phoneInput.setValue(value);
    }

    enterSiteURL(value) {
        page.siteURLInput.waitForDisplayed(2000);
        page.siteURLInput.setValue(value);
    }

    enterDescription(value) {
        page.descriptionInput.waitForDisplayed(2000);
        page.descriptionInput.setValue(value);
    }

    nextStepButton() {
        page.buttonNext.waitForDisplayed(3000);
        page.buttonNext.click();
    }

    uploadImagePlace(path) {
        page.uploadImagePlaceInput.waitForExist(2000);
        page.uploadImagePlaceInput.setValue(path);
    }

    openCategory(){
        page.openCategory.waitForExist(2000);
        page.openCategory.click();
    }

    selectCategory() {
        page.selectCategory.waitForExist(2000);
        page.selectCategory.click();
    }

    openTag() {
        page.openTag.waitForExist(2000);
        page.openTag.click();
    }

    selectTag() {
        page.selectTag.waitForExist(2000);
        page.selectTag.click();
    }

    checkFeature() {
        page.checkFeature.waitForExist(2000);
        page.checkFeature.click();
    }

    getNamePlace (){
        //page.getNamePlace.waitForExist(2000);
        return page.getNamePlace.getText()
    }
}

module.exports = NewPlaceActions;
