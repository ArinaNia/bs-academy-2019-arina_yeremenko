const NewPlaceActions = require('./UserNewPlace/actions/UserNewPlace_pa');
const MenuActions = require('./Menu/pages/menu_pa');
const HelpClass = require('../helpers/helpers');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const credentials = require('./testData.json');

const menuSteps = new MenuActions();
const pageSteps = new NewPlaceActions();
const helpSteps = new HelpClass();


describe('New place page tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        helpSteps.loginWithDefaultUser();
        wait.forSpinner();
        menuSteps.navigateToNewPlace();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    xit('should create new place', () => {

        const namePlace = helpSteps.createRandomName(credentials.placeName);
        pageSteps.enterPlaceName(namePlace);
        pageSteps.enterZip(credentials.zip);
        pageSteps.enterAddress(credentials.addressPlace);
        pageSteps.enterPhone(credentials.phonePlace);
        pageSteps.enterSiteURL(credentials.websiteURL);
        pageSteps.enterDescription(credentials.descriptionPlace);
        pageSteps.nextStepButton();

        pageSteps.uploadImagePlace(helpSteps.getPathWithRandomImage(credentials.imageList));
        pageSteps.nextStepButton();

        pageSteps.nextStepButton();


        pageSteps.openCategory();
        pageSteps.selectCategory();
        pageSteps.openTag();
        pageSteps.selectTag();
        pageSteps.nextStepButton();


        pageSteps.checkFeature();
        pageSteps.nextStepButton();

        pageSteps.nextStepButton();

        pageSteps.nextStepButton();


        validators.nameValue(pageSteps.getNamePlace(), namePlace);
    });

});