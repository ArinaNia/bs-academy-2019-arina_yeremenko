const SettingsActions = require('./UserSettings/actions/UserSettings_pa');
const MenuActions = require('./Menu/pages/menu_pa');
const HelpClass = require('../helpers/helpers');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const credentials = require('./testData.json');

const menuSteps = new MenuActions();
const pageSteps = new SettingsActions();
const helpSteps = new HelpClass();

describe('Settings page tests', () => {

    beforeEach(() => {
        helpSteps.loginWithDefaultUser();
        wait.forSpinner();
        menuSteps.navigateToSettings();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should login with changed password', () => {

        pageSteps.enterOldPassword(credentials.password);
        pageSteps.enterNewPassword(credentials.changedPassword);
        pageSteps.saveChanges();

        validators.successNotificationTextIs(credentials.changedDataNotification);
        wait.forNotificationToDisappear();

        menuSteps.logOut();
        helpSteps.loginWithCustomUser(credentials.email, credentials.password);

        validators.errorNotificationTextIs(credentials.loginErrorNotification);
        wait.forNotificationToDisappear();

        browser.reloadSession();
        helpSteps.loginWithCustomUser(credentials.email, credentials.changedPassword);
        wait.forSpinner();
        menuSteps.navigateToSettings();

        pageSteps.enterOldPassword(credentials.changedPassword);
        pageSteps.enterNewPassword(credentials.password);
        pageSteps.saveChanges();

    });

    it('should edit user first name', () => {
        pageSteps.enterFirstName(credentials.changedName);
        pageSteps.enterLastName(credentials.changedSurname);
        pageSteps.saveChanges();

        browser.refresh();

        validators.nameValue(pageSteps.getFristNameValue() ,credentials.changedName);
        validators.nameValue(pageSteps.getLastNameValue(), credentials.changedSurname);

        pageSteps.enterFirstName(credentials.name);
        pageSteps.enterLastName(credentials.surname);
        pageSteps.saveChanges();

    });


});