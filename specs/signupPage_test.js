const SignUpActions = require('./SignUp/actions/UserSignUp_pa');
const HelpClass = require('../helpers/helpers');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const credentials = require('./testData.json');

const pageSteps = new SignUpActions();
const helpSteps = new HelpClass();

describe('SignUp page tests', () => {

    beforeEach(() => {
        helpSteps.goSignUpPage();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should sign up with valid data', () => {

        pageSteps.enterFirstName(credentials.firstName);
        pageSteps.enterLastName(credentials.lastName);
        pageSteps.enterEmail(helpSteps.createRandomEmail());
        pageSteps.enterNewPassword(credentials.password);

        pageSteps.createUserButton();

        validators.successNotificationTextIs(credentials.signUpNotificationSuccess);
        wait.forNotificationToDisappear();
    });

});