const PlaceActions = require('./Place/actions/UserPlace_pa');
const HelpClass = require('../helpers/helpers');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const credentials = require('./testData.json');

const pageSteps = new PlaceActions();
const helpSteps = new HelpClass();


describe('Place page tests', () => {

    beforeEach(() => {
        helpSteps.loginWithDefaultUser();
        wait.forSpinner();
        helpSteps.goURLFirstPlace();
        wait.forSpinner();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should add rate place', () => {

        pageSteps.ratePlace();
        wait.forModalDisplayed();
        pageSteps.markPlace();

        validators.successNotificationTextIs(credentials.ratingSetNotification);
        wait.forNotificationToDisappear();

    });

    it('should post place review', () => {

        const textReview = helpSteps.createRandomName(credentials.reviewText);
        pageSteps.addReviewPlace(textReview);
        pageSteps.postReviewPlace();

        validators.successNotificationTextIs(credentials.addReviewPlaceNotification);
        wait.forNotificationToDisappear();

        validators.nameValue(helpSteps.getReviewText(), textReview)

    });

    it('should check in place', () => {
        const numberBeforeCheckIn = pageSteps.getNumberCheckIn();
        pageSteps.checkInPlace();


        validators.successNotificationTextIs(credentials.checkInNotification);
        wait.forNotificationToDisappear();

        const numberAfterCheckIn = pageSteps.getNumberCheckIn();

        validators.nameValue(parseInt(numberBeforeCheckIn) + 1, parseInt(numberAfterCheckIn))

    });

    it('should like review place', () => {
        pageSteps.addLikeReviewPlace();

        validators.errorNotificationTextIs(credentials.addLikeReviewErrorNotification);
        wait.forNotificationToDisappear();

    });

});