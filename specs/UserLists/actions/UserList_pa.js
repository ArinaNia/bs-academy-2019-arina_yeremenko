const ListsPage = require('../page/UserLists_po');
const page = new ListsPage();

class ListsActions {

    enterListName(value) {
        page.listNameInput.waitForDisplayed(2000);
        page.listNameInput.clearValue();
        page.listNameInput.setValue(value);
    }

    enterPlaceInput(value) {
        page.searchPlaceInput.waitForDisplayed(2000);
        page.searchPlaceInput.setValue(value);
    }

    uploadImageList(path) {
        page.imageListInput.waitForDisplayed(2000);
        page.imageListInput.setValue(path);
    }

    deleteImageList() {
        page.deleteImageListButton.waitForDisplayed(2000);
        page.deleteImageListButton.click();
    }

    saveNewList() {
        page.buttonSaveList.waitForDisplayed(2000);
        page.buttonSaveList.click();
    }

    updateList(){
        page.updateListButton.waitForExist(2000);
        page.updateListButton.click();
    }
    addNewList(){
        page.buttonAddNewList.waitForDisplayed(2000);
        browser.execute(() => {
            document.querySelectorAll('a.button')[0].click();
        })
    }

    choicePlace(){
        page.choicePlace.waitForExist(3000);
        page.choicePlace.click();
    }


    deleteList(){
        //page.deleteListButton.waitForExist(2000);
        page.deleteListButton.click();
    }

    deleteModal() {
        page.deleteModal.waitForDisplayed(2000);
    }

    approveDeleteList (){
        page.deleteModalButton.waitForDisplayed(2000);
        page.deleteModalButton.click();
    }

    getNameFirstList (){
        //page.firstListItem.waitForDisplayed(2000);
        return page.firstListItem.getText()
    }
}

module.exports = ListsActions;
