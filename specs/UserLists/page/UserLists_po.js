class ListsPage {

    get buttonAddNewList () {return $('a[href="/my-lists/add"]')};
    get listNameInput () {return $('#list-name')};
    get searchPlaceInput () {return $('input.search-field.input')};
    get choicePlace () {return $('div.search-places__list > ul > li')};
    get imageListInput () {return $('input.file-input')};
    get buttonSaveList () {return $('button.is-success')};
    get deleteImageListButton () {return $('//a/span[contains(., "Delete image")]')};
    get deleteListButton () {return $('button.button.is-danger')};
    get deleteModal () {return $('div.animation-content.modal-content')};
    get deleteModalButton () {return $('div.buttons.is-centered > button.button.is-danger')};
    get firstListItem () {return $('.title.has-text-primary')};
    get updateListButton () {return $('//button[contains(., "Update")]')}
};

module.exports = ListsPage;