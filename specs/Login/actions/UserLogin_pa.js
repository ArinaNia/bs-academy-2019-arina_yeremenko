const LoginPage = require('../page/UserLogin_po');
const page = new LoginPage();

class LoginActions {

    enterEmail(value) {
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.setValue(value);
    }

    enterPassword(value) {
        page.passwordInput.waitForDisplayed(2000);
        page.passwordInput.setValue(value);
    }

    loginButton () {
        page.loginButton.waitForDisplayed(2000);
        page.loginButton.click();
    }

    linkSignUp () {
        page.linkSignUp.waitForDisplayed(2000);
        page.linkSignUp.click();
    }

}

module.exports = LoginActions;
