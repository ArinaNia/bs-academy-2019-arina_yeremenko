class SignUpPage {

    get firstNameInput () {return $('input[name="firstName"]')};
    get lsatNamedInput() {return $('input[name="lastName"]')};
    get emailInput () {return $('input[name="email"]')};
    get newPasswordInput () {return $('input[type="password"]')};
    get buttonCreateUser () {return $('button.button.is-primary')}

};

module.exports = SignUpPage;