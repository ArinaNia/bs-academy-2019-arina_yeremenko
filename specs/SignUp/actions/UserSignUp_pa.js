const SignUpPage = require('../page/UserSignUp_po');
const page = new SignUpPage();

class SignUpActions {

    enterFirstName(value) {
        page.firstNameInput.waitForDisplayed(2000);
        page.firstNameInput.setValue(value);
    }

    enterLastName(value) {
        page.lsatNamedInput.waitForDisplayed(2000);
        page.lsatNamedInput.setValue(value);
    }

    enterEmail(value) {
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.setValue(value);
    }

    enterNewPassword(value) {
        page.newPasswordInput.waitForDisplayed(2000);
        page.newPasswordInput.setValue(value);
    }

    createUserButton () {
        page.buttonCreateUser.waitForDisplayed(2000);
        page.buttonCreateUser.click();
    }

}

module.exports =SignUpActions;
