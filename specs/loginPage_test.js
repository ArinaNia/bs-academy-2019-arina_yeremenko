const LoginActions = require('./Login/actions/UserLogin_pa');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const credentials = require('./testData.json');

const pageSteps = new LoginActions();


describe('Login page tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should login with valid data', () => {
        pageSteps.enterEmail(credentials.email);
        pageSteps.enterPassword(credentials.password);

        pageSteps.loginButton();

        validators.successNotificationTextIs(credentials.loginNotification);
        wait.forNotificationToDisappear();

    });


});