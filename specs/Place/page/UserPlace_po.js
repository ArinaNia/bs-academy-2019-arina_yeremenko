class PlacePage {

    get ratingPlaceButton () {return $('button.is-primary.rating')};
    get chekinPlaceButton () {return $('button.is-primary.checkin')};
    get checkPlaceMark () {return $('.far.fa-2x.fa-angry')};
    get reviewTextarea () {return $('textarea')};
    get reviewPostButton () {return $('//button[contains(., "Post")]')};
    get likeDislikeIcon () {return $("i.fa.fa-heart.fa-stack-1x")}

};

module.exports = PlacePage;