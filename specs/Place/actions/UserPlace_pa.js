const PlacePage = require('../page/UserPlace_po');
const page = new PlacePage();

class PlaceActions {


    ratePlace(){
        page.ratingPlaceButton.waitForDisplayed(2000);
        page.ratingPlaceButton.click();
    }

    markPlace() {
        page.checkPlaceMark.waitForExist(2000);
        page.checkPlaceMark.click();
    }

    checkInPlace(){
        page.chekinPlaceButton.waitForExist(2000);
        page.chekinPlaceButton.click();
    }

    getNumberCheckIn(){
        return page.chekinPlaceButton.getText();
    }

    addReviewPlace(value) {
        page.reviewTextarea.waitForDisplayed(2000);
        page.reviewTextarea.setValue(value);
    }

    postReviewPlace() {
        page.reviewPostButton.waitForDisplayed(2000);
        page.reviewPostButton.click();
    }

    addLikeReviewPlace() {
        page.likeDislikeIcon.waitForDisplayed(2000);
        page.likeDislikeIcon.click();
    }

}

module.exports = PlaceActions;
