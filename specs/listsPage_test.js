const ListsActions = require('./UserLists/actions/UserList_pa');
const HelpClass = require('../helpers/helpers');
const wait = require('../helpers/waiters');
const validators = require('../helpers/validators');
const MenuActions = require('./Menu/pages/menu_pa');
const credentials = require('./testData.json');

const menuSteps = new MenuActions();
const pageSteps = new ListsActions();
const helpSteps = new HelpClass();

describe('Lists page tests', () => {

    beforeEach(() => {
        helpSteps.loginWithDefaultUser();
        wait.forSpinner();
        menuSteps.navigateToLists();
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create new list', () => {
        pageSteps.addNewList();

        const nameList = helpSteps.createRandomName(credentials.listName);
        pageSteps.enterListName(nameList);
        pageSteps.uploadImageList(helpSteps.getPathWithRandomImage(credentials.imageList));
        pageSteps.enterPlaceInput(credentials.searchPlace);
        pageSteps.choicePlace();
        pageSteps.saveNewList();
        wait.forNotificationToDisappear();

        validators.successNotificationTextIs(credentials.createListNotification);
        validators.nameValue(pageSteps.getNameFirstList(), nameList);

    });

    it('should delete list', () => {
        pageSteps.deleteList();
        pageSteps.deleteModal();
        pageSteps.approveDeleteList();
        wait.forSpinner();

        validators.isinfoNotificationTextIs(credentials.deleteListNotification);
        wait.forNotificationToDisappear();

    });

    it('should edit list', () => {

        const newNameList = helpSteps.createRandomName(credentials.listNewName);
        helpSteps.selectItemInList(credentials.nameButtonEdit);
        wait.forSpinner();
        pageSteps.enterListName(newNameList);
        pageSteps.deleteImageList();
        pageSteps.updateList();
        wait.forSpinner();

        validators.successNotificationTextIs(credentials.updateListNotification);
        wait.forNotificationToDisappear();
        validators.nameValue(pageSteps.getNameFirstList(), newNameList);
    });

    // xit('should get place info' , () => {
    //     helpSteps.clickItemInList('Name list 156');
    // })

});