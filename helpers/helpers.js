const LoginActions = require('../specs/Login/actions/UserLogin_pa');
const page = new LoginActions();
const credentials = require('../specs/testData.json');
const path = require('path');
const url = require('url');

class HelpClass 
{

    clickItemInList(name) {
        const place = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]`);
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        place[0].scrollIntoView();
        place[0].click();
    }

    goURLFirstPlace () {
        const arr = $(`//h3/a[contains(@href, 'places/')]`);
        browser.url(arr.getAttribute('href'));
    }

    selectItemInList (nameButton) {
        const editButton = $(`//a[contains(@href, "${nameButton}")]`);
        const urlpath = url.parse(editButton.getAttribute('href'));
        const place = $(`//a[@href = "${urlpath.pathname}"]`);
        place.click();
    }

    getReviewText() {
        //const paragraph = $ (`/div/p[contains(., "${text}")]`);
        const paragraph = $('div.content > p');
        return paragraph.getText();
    }

    getPathWithRandomImage(arrayImage) {
        const randomImage = Math.floor(Math.random() * arrayImage.length);
        return path.join(__dirname, 'images', arrayImage[randomImage]);
    }

    createRandomName(patternName){
        return patternName + ' ' + Math.round(Math.random() * 500)
    }

    createRandomEmail(){
        return Math.random().toString(36).substring(3, 10) + "@gmail.com";
    }

    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.loginButton();
    }
    
    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');

        page.enterEmail(email);
        page.enterPassword(password);
        page.loginButton();
    }
    goSignUpPage() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');
        page.linkSignUp();
    }


}

module.exports = HelpClass;