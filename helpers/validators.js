const assert = require('chai').assert;
const expect = require('chai').expect;

class AssertHelper {

    

    elementCountIs(locator, expectedQty) {
        const els = locator;
        const actualQty = els.length;

        assert.strictEqual(actualQty, expectedQty, `Expected ${expectedQty} is not equal to ${actualQty}`);
    }

    nameValue(actualName, expectedName){
        assert.strictEqual(expectedName, actualName, `Expected ${expectedName} is not equal to ${expectedName}`);
    }

    wrongValueIndicationOnField(locator) {
        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include validation class`).to.include("is-danger");
    }

    wrongValueIndicationOnLable(locator) {

        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include error class`).to.include("error");
    }

    errorNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-danger div');
        const actualText = notification.getText();
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    successNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-success div');
        const actualText = notification.getText();
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    isinfoNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-info div');
        const actualText = notification.getText();
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }


}

module.exports = new AssertHelper();